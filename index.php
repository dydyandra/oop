<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep = new Animal("shaun");

    // echo $sheep->name;
    // echo "<br>"; // "shaun"
    // echo $sheep->legs; // 4
    // echo "<br>";
    // echo $sheep->cold_blooded; // "no"

    echo "<br>Name: " . $sheep->get_name() . "<br>";
    echo "Legs: " . $sheep->get_legs() . "<br>";
    echo "Cold blooded: " . $sheep->get_cold_blooded() . "<br>";

    $kodok = new Frog("buduk");
    echo "<br>Name: " . $kodok->get_name() . "<br>";
    echo "Legs: " . $kodok->get_legs() . "<br>";
    echo "Cold blooded: " . $kodok->get_cold_blooded() . "<br>";
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "<br><br>Name: " . $sungokong->get_name() . "<br>";
    echo "Legs: " . $sungokong->get_legs() . "<br>";
    echo "Cold blooded: " . $sungokong->get_cold_blooded() . "<br>";
    $sungokong->yell(); // "Auooo"
 ?>